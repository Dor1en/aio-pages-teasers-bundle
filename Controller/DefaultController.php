<?php

namespace Aio\Bundle\PagesTeasersBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em ;
	
	private $area_list;
	private $repo_sitemapurl;
	private $links_url;

    protected function getRepository($repository = 'AioPagesTeasersBundle:Area')
    {
        return $this->em->getRepository($repository) ;
    }
	
	public function indexAction($id, $lang)
    {
    	$em = $this->getDoctrine()->getManager();
    	$repo_site = $em->getRepository('AioCoreBundle:Site');
		$repo_sitemapurl = $em->getRepository('AioSitemapUrlBundle:SitemapUrl');
		
		$this->setAreaList();
		$this->setRepoSitemapUrl();
		$this->setLinks($id, $lang);
		
		$sites = $repo_site->findAll();
		$mySite = $repo_site->find($id);
		
		$links = $this->create_lvl_url($id, $lang);
		
	  	return $this->render('AioPagesTeasersBundle:Default:index.html.twig', array(
        	'links' => $links['tree'],
        	'links_info' => $links['info'],
        	'sites' => $sites,
        	'site_name' => $mySite->getDomain(),
        	'id' => $id, 
        	'list_lang' => $repo_sitemapurl->groupByLang($id),
        	'lang' => $lang
		));
	}
	
    public function contentAction($id, $lang)
    {
    	$repo_site = $this->getRepository('AioCoreBundle:Site');
		$repo_sitemapurl = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');
		
		$this->setAreaList();
		$this->setRepoSitemapUrl();
		$this->setLinks($id, $lang);
		
		$areas_list = $this->getAreaList();
			
		$sites = $repo_site->findAll();
		$mySite = $repo_site->find($id);
		
		$links = $this->create_lvl_url($id, $lang);
		
        return $this->render('AioPagesTeasersBundle:Default:content.html.twig', array(
        	'links' => $links['tree'],
        	'links_info' => $links['info'],
        	'sites' => $sites,
        	'site_name' => $mySite->getDomain(),
        	'areas' => $areas_list,
        	'id' => $id,
        	'list_lang' => $repo_sitemapurl->groupByLang($id),
        	'lang' => $lang,
        	//'rubriques' => $myArea->getTree()
		));
    }

	
    public function structureAction($id, $lang)
    {
   		$repo_site = $this->getRepository('AioCoreBundle:Site');
		$repo_sitemapurl = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');
		$area = $this->getRepository();
		
		$this->setAreaList();
		$this->setRepoSitemapUrl();
		$this->setLinks($id, $lang);
		
		$sites = $repo_site->findAll();
		$mySite = $repo_site->find($id);
		
		$links = $this->create_lvl_url($id, $lang);
		
        return $this->render('AioPagesTeasersBundle:Default:structure.html.twig', array(
        	'links' => $links['tree'],
        	'links_info' => $links['info'],
        	'sites' => $sites,
        	'site_name' => $mySite->getDomain(),
        	'areas' => $area->findAll(),
        	'id' => $id,
        	'list_lang' => $repo_sitemapurl->groupByLang($id),
        	'lang' => $lang,
		));
    }


	//Méthode ************************************************************
	
	//Méthodes pour créer un tableau mutli-dimensionnel suivant le lien( url/news/detail/news-1-02-13 = arr[url][news][detail][news-01-02-13])
	function create_lvl_url($id, $lang)
	{
    	$repo_sitemapurl = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');
		$repo_site = $this->getRepository('AioCoreBundle:Site');
		
		//Récupère la liste des liens triés par langue
		$links = $this->getLinks();

		$links_tree = array();
		$links_domain = array();
		$i = 0;	
		
		foreach ($links as $link) 
		{
			//Récuère le domaine avec le lien du sitemap
			$domain = str_replace('sitemap.xml', '', $link['url']);
			
			//Enlève les informations inutiles comme /{lang} dans l'url
			$key = explode("/", $link['su_url']);
			$lang = $key[1];
			unset($key[0]);
			unset($key[1]);
			
			foreach ($key as $index => $value) 
			{
				if(empty($value))
				{
					unset($key[$index]);
				}
			}
			
			//Reset des index du tableau php
			$key = array_values($key);
			//Création d'un tableau mutli-dimensionnel suivant le lien( url/news/detail/news-1-02-13 = arr[url][news][detail][news-01-02-13])
			if(isset($key[0]))
			{
				if(!isset($links_tree[$key[0]]))
				{
					$links_tree[$key[0]] = array();
					
					$links_domain[$key[0]] = null;
					$links_domain[$key[0]]['domain'] = $domain;
					$links_domain[$key[0]]['lang'] = $lang;
					
					$this->checkUrl($id, $key, $lang, 0, $links_domain);
				}
						
				$this->child_level($links_tree[$key[0]], $key, 1, $domain, $links_domain[$key[0]], $lang, $id);
				
			}
		}
		
		
		return array('tree' => $links_tree, 'info' => $links_domain);
	}
	
	function child_level(&$arr, $key, $index, $domain, &$arr_domain, $lang, $site_id)
	{
		if(isset($key[$index]))
		{
			if(!isset($arr[$key[$index]]))
			{
				$arr[$key[$index]] = array();
				
				$arr_domain[$key[$index]] = null;
				$arr_domain[$key[$index]]['domain'] = $domain;
				$arr_domain[$key[$index]]['lang'] = $lang;
				
				//check si l'url existe en base de données
				$this->checkUrl($site_id, $key, $lang, $index, $arr_domain);
			}
			
			$this->child_level($arr[$key[$index]], $key, $index+1, $domain, $arr_domain[$key[$index]], $lang, $site_id);
		}
	}

	/* Fonction pour check si une url existe en base de données 
	 * Si elle existe on ajoute une info dans le tableau des URLS
	 */
	function checkUrl($site_id, $key, $lang, $index, &$arr_domain)
	{
    	$repo_sitemapurl = $this->getRepoSitemapUrl();
		$repo_urlContent = $this->getRepository('AioPagesTeasersBundle:SitemapUrlContent');

		//$area_list = $this->getAreaList();
		
		//Création de l'url pour check si elle existe en base de données
		$url = "/" . $lang;
	
		for($i = 0; $i <= $index; $i++)
		{
			$url .= "/" . $key[$i];	
		}

		$checkUrl  = $this->findUrl($url);

		//Check si l'url existe
		if(empty($checkUrl))
		{
			$checkUrl  = $this->findUrl($url . "/");
			if(empty($checkUrl))
			{
				$arr_domain[$key[$index]]['endurl'] = false;
			}
			else //si elle existe
			{
				$arr_domain[$key[$index]]['endurl'] = true;	

				//$url_list = $repo_urlContent->findBySitemapUrl($checkUrl['id']);
				
				$area_id = 0;
				if(isset($url_list[0]))
				{
					$myArea = $url_list[0]->getArea();
					$area_id = $myArea->getId();
				}
				
				$arr_domain[$key[$index]][$area_id]['isSet'] = true;//count($url_list);
				$arr_domain[$key[$index]]['url_id'] = $checkUrl['id'];
				
				//Récupère les zone de l'url
				$url = $this->getRepoSitemapUrl();
				$url = $url->find($checkUrl['id']);
				
				foreach ($url->getArea() as $key => $area) 
				{
					$arr_domain[$key[$index]][$area->getId()]['area'] = true; 
				}
			}
		}
		else //si elle existe
		{
			$arr_domain[$key[$index]]['endurl'] = true;
			
			/*foreach ($this->getAreaList() as $key => $area) {
				
				//$url_list =  $repo_urlContent->findBy(array('sitemapUrl' => $checkUrl['id'], 'area' => $area->getId()));
			}*/
			
			//$url_list = $repo_urlContent->findBySitemapUrl($checkUrl['id']);
			
			$area_id = 0;
			if(isset($url_list[0]))
			{
				$myArea = $url_list[0]->getArea();
				$area_id = $myArea->getId();
			}
			
			$arr_domain[$key[$index]][$area_id]['isSet'] = true;//count($url_list);
			$arr_domain[$key[$index]]['url_id'] = $checkUrl['id'];
			
			//Récupère les zone de l'url
			$url = $this->getRepoSitemapUrl();
			$url = $url->find($checkUrl['id']);
			foreach ($url->getArea() as $key => $area) 
			{
				//$arr_domain[$key[$index]][$area->getId()]['area'] = true; 
			}

		}
	}

	function getListLang($site_id)
	{
    	$em = $this->getDoctrine()->getManager();
		$repo_sitemapurl = $em->getRepository('AioSitemapUrlBundle:SitemapUrl');
		
		return $repo_sitemapurl->groupByLang($site_id);
	}
	
	function setAreaList()
	{
		$area = $this->getRepository();
		$this->area_list = $area->findAll();
	}
	
	function getAreaList()
	{
		return $this->area_list;
	}
	
	function setRepoSitemapUrl()
	{
		$this->repo_sitemapurl = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');
	}
	
	function getRepoSitemapUrl()
	{
		return $this->repo_sitemapurl;
	}
	
	function setLinks($id, $lang)
	{
		$this->links_url =  $this->getRepoSitemapUrl()->findBySiteAndLang($id, $lang);
	}
	
	function getLinks()
	{
		return $this->links_url;
	}
	
	function findUrl($url)
	{
		$result = null;
		
		$links = $this->getLinks();		
		foreach ($links as $key => $link) 
		{
			if($link['su_url'] == $url)
			{
				$result = $link;
				break;
			}		
		}
		
		return $result;
	}

}
