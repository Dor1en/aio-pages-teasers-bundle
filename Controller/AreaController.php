<?php

namespace Aio\Bundle\PagesTeasersBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Aio\Bundle\PagesTeasersBundle\Entity\Area;
use Aio\Bundle\PagesTeasersBundle\Entity\SitemapUrlContent;

use Aio\Bundle\PagesTeasersBundle\Form\AreaType;

class AreaController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em ;

    protected function getRepository($repository = 'AioPagesTeasersBundle:Area')
    {
        return $this->em->getRepository($repository) ;
    }
	
	public function indexAction($id, $lang)
    {
    	$area = $this->getRepository();
		
	 	return $this->render('AioPagesTeasersBundle:Area:index.html.twig', array(
	 		'areas' => $area->findAll(),
        	'lang' => $lang,
        	'id' => $id,
		));
	}
	
	public function addAction($id, $lang)
	{
	 	$request = $this->get('request');
	 	$myArea = new Area();
	 	$form = $this->container->get('form.factory')->create(new AreaType(),  $myArea);
		
	 	if($request->getMethod() == 'POST') //Récupère les informations du Formulaire pour ajouter une zone
	 	{
 		 	$form->bind($request) ;

            $this->em->persist($myArea) ;
            $this->em->flush() ;

            return $this->redirect($this->generateUrl('aio_pages_teasers_area_edit', array(
            	'lang' => $lang,
            	'id' => $id,
            	'area_id' => $myArea->getId(),
			)));
		} 
		else
		{
			return $this->render('AioPagesTeasersBundle:Area:add.html.twig', array(
				'form' => $form->createView(),
				'lang' => $lang,
				'id' => $id,
			));
		}
	}
	
	public function editAction($id, $lang, $area_id)
    {
		$request = $this->get('request');
 	 	$area = $this->getRepository();
		  
	 	$myArea = $area->find($area_id);
	 	$form = $this->container->get('form.factory')->create(new AreaType(),  $myArea);
		
	 	if($request->getMethod() == 'POST') //Récupère les informations du Formulaire pour modifier une zone
	 	{
 		  	$form->bind($request) ;

            //Modification du lien dans la base de données
            $this->em->persist($myArea);
            $this->em->flush();
		}

		return $this->render('AioPagesTeasersBundle:Area:edit.html.twig', array(
			'form' => $form->createView(),
			'lang' => $lang,
			'id' => $id,
			'area_id' => $area_id,
			'rubriques' => $myArea->getTree(),//Récupère la liste des rubriques
		));
	}
	
	public function deleteAction($id, $lang, $area_id)
	{
		$area = $this->getRepository();
			
		//Récupère la zone à supprimer
        $myArea = $area->find($area_id);

        $this->em->remove($myArea);
        $this->em->flush();
			
	   	return $this->redirect($this->generateUrl('aio_pages_teasers_area', array(
    		'lang' => $lang,
        	'id' => $id,
		)));
	}

	public function rubriqueItemsAction($lang, $area_id)
	{
		$request = $this->get('request');
		$rubrique_id = $request->request->get('rubrique_id'); 
		
		$area = $this->getRepository();	
		$myArea = $area->find($area_id);
		
		if(!isset($rubrique_id))
		{
			$rubrique = $myArea->getTree();
			
			foreach ($rubrique as $value) 
			{
				$rubrique_id = $value->getId();
				break;
			}
		}
				
		$tree = $this->getRepository('AioContentBundle:Tree')->find($rubrique_id);
		
		if(!$rubrique_content = $this->get('aio.content.tree')->search($tree, $lang, $tree->getLanguages(), null, null, null, null, null, null, null, null)) {
                throw new \DomainException('An error occured while database querying!') ;
        }
		
		$content_save = $myArea->getContent();
		$arr_content_save = array();
		foreach ($rubrique_content['records'] as $key => $value) 
		{
			foreach ($content_save as $key_save => $value_save) 
			{
				if($value_save->getId() == $value['id'])
				{
					$arr_content_save[] = $value;
					unset($rubrique_content['records'][$key]);
				}
			}
		}
		
		return $this->render('AioPagesTeasersBundle:Area:rubrique_items_area.html.twig', array(
			'rubrique_content' => $rubrique_content,
			'content_save' => $arr_content_save,
		));
	}

	public function rubriqueItemsUrlAction($lang, $area_id, $url_id = 0, $id)
	{
		$request = $this->get('request');
		$rubrique_id = $request->request->get('rubrique_id'); 
		
		$sitemap_url = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');	
		$area = $this->getRepository();	
		
		$repo_urlContent = $this->getRepository('AioPagesTeasersBundle:SitemapUrlContent');
		
		$myUrl = $sitemap_url->find($url_id);
		$myArea = $area->find($area_id);

		if(!isset($rubrique_id))
		{
			$rubrique = $myArea->getTree();
			
			foreach ($rubrique as $value) 
			{
				$rubrique_id = $value->getId();
				break;
			}
		}
		
		$tree = $this->getRepository('AioContentBundle:Tree')->find($rubrique_id);
		
		if(!$rubrique_content = $this->get('aio.content.tree')->search($tree, $lang, $tree->getLanguages(), null, null, null, null, null, null, null, null)) {
                throw new \DomainException('An error occured while database querying!') ;
        }
		
		if(isset($myUrl))
		{
			//$content_save = $myUrl->getContent(); 
			$content_save = $repo_urlContent->findBy(array('sitemapUrl' => $myUrl, 'area' => $myArea));
			
			foreach ($content_save as $key => $value) 
			{
				$content_save[$key] = $value->getContent();
			}
			
			//Si aucune accoche a été définie sur une url spécifique, on récupère les accroche par défault de la zone
			if(count($content_save) == 0)
			{
				$content_save = $myArea->getContent();
			}
		}
		else
		{
			$content_save = $myArea->getContent();	
		}
		
		
		$arr_content_save = array();
		foreach ($rubrique_content['records'] as $key => $value) 
		{
			foreach ($content_save as $key_save => $value_save) 
			{
				if($value_save->getId() == $value['id'])
				{
					$arr_content_save[] = $value;
					unset($rubrique_content['records'][$key]);
				}
			}
		}
		return $this->render('AioPagesTeasersBundle:Area:rubrique_items.html.twig', array(
			'rubrique_content' => $rubrique_content,
			'content_save' => $arr_content_save,
			'area' => $myArea,
			'id' => $id, 
			'url_id' => $url_id,
			'key_url' => $myUrl->getKey(),
		));
	}
	
	public function areaSaveContentAction($area_id, $id)
	{
		$area = $this->getRepository();	
		$myArea = $area->find($area_id);
		$content = $this->getRepository('AioContentBundle:Content');
		
		//Lie les items à la zone
		if(isset($_POST['content_items']))
		{
			$content_items = $_POST['content_items'];
			
			foreach ($content_items as $value)
			{
				$myContent = $content->find($value);
				$myArea->addContent($myContent);
			}
			
			try
			{
				$this->em->persist($myArea); 
			 	$this->em->flush();
			}
			catch(\Exception $e){}
		}

		//Retire un item à la zone
		if(isset($_POST['content_items_remove']))
		{
			$content_items_remove = $_POST['content_items_remove'];
			
			foreach ($content_items_remove as $value)
			{
				$myContent = $content->find($value);
				$myArea->removeContent($myContent);
			}
			
			try
			{
				$this->em->persist($myArea); 
			 	$this->em->flush();
			}
			catch(\Exception $e){}
		}
		
	 	 return $this->redirect($this->generateUrl('aio_pages_teasers_area_edit', array(
        	'lang' => 'fr',
        	'id' => $id,
        	'area_id' => $myArea->getId(),
		)));
	}

	public function urlSaveContentAction($key_url, $area_id, $id)
	{
		$sitemap_url = $this->getRepository('AioSitemapUrlBundle:SitemapUrl');	
		$content = $this->getRepository('AioContentBundle:Content');
		$url_content = $this->getRepository('AioPagesTeasersBundle:SitemapUrlContent');
		$area = $this->getRepository();	
		$urls_list = $sitemap_url->findByKey($key_url);
		
		//Lie les items à l'url
		if(isset($_POST['content_items_'.$area_id]))
		{
			$content_items = $_POST['content_items_'.$area_id];
			foreach ($urls_list as $myUrl)
		 	{
				foreach ($content_items as $value)
				{
					$myUrlContent = new SitemapUrlContent();
					$myContent = $content->find($value);
					$myArea = $area->find($area_id);
					
					$myUrlContent->setSitemapUrl($myUrl);
					$myUrlContent->setContent($myContent);
					$myUrlContent->setArea($myArea);
					
					$this->em->persist($myUrlContent); 
			 		$this->em->flush();	
					
					/*$myContent = $content->find($value);
					$myUrl->addContent($myContent);*/
				}
			}
		}
			
		//Supprime les items de l'url
		if(isset($_POST['content_items_remove_'.$area_id]))
		{
			$content_items_remove = $_POST['content_items_remove_'.$area_id];
			foreach ($urls_list as $myUrl)
		 	{
		 		foreach ($content_items_remove as $value)
				{
					$myContent = $content->find($value);
					$myArea = $area->find($area_id);
					
					$myUrlContent = $url_content->findOneBy(array(
						'content' => $myContent,
						'sitemapUrl' => $myUrl
					));
					
					try
					{
						$this->em->remove($myUrlContent); 
				 		$this->em->flush();
					}
					catch(\Exception $e){}
				}
			}
		}
		
 	 	return $this->redirect($this->generateUrl('aio_pages_teasers_content', array(
        	'lang' => 'fr',
        	'id' => $id,
		)));
	}
}
