<?php

namespace Aio\Bundle\PagesTeasersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aio\Bundle\PagesTeasersBundle\Entity\SitemapUrlContent
 *
 * @ORM\Table(name="Sitemap_Url_Content")
 * @ORM\Entity
 */
class SitemapUrlContent
{
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Aio\Bundle\SitemapUrlBundle\Entity\SitemapUrl")
	 * @ORM\JoinColumn(name="sitemap_url_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $sitemapUrl;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Aio\Bundle\ContentBundle\Entity\Content")
	 * @ORM\JoinColumn(name="content_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $content;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Aio\Bundle\PagesTeasersBundle\Entity\Area")
	 * @ORM\JoinColumn(name="area_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $area;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setSitemapUrl(\Aio\Bundle\SitemapUrlBundle\Entity\SitemapUrl $sitemapUrl)
	{
		$this->sitemapUrl = $sitemapUrl;
	}
	
	public function getSitemapUrl()
	{
		return $this->sitemapUrl;
	}
	
	public function setContent(\Aio\Bundle\ContentBundle\Entity\Content $content)
	{
		$this->content = $content;
	}
	
	public function getContent()
	{
		return $this->content;
	}
	
	public function setArea(\Aio\Bundle\PagesTeasersBundle\Entity\Area $area)
	{
		$this->area = $area;
	}
	
	public function getArea()
	{
		return $this->area;
	}
	
}