<?php

namespace Aio\Bundle\PagesTeasersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aio\Bundle\PagesTeasersBundle\Entity\Area
 *
 * @ORM\Table(name="Area")
 * @ORM\Entity
 */
class Area
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Aio\Bundle\ContentBundle\Entity\Tree", cascade={"persist"})
   	 */
  	private $tree;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Aio\Bundle\ContentBundle\Entity\Content", cascade={"persist"})
	 */
	private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

 	public function __construct()
  	{
	    $this->tree = new \Doctrine\Common\Collections\ArrayCollection();
		$this->content = new \Doctrine\Common\Collections\ArrayCollection();
  	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
   /**
	* Add tree
	*
	* @param Aio\ContentBundle\Entity\tree $tree
	*/
	public function addTree(\Aio\Bundle\ContentBundle\Entity\tree $tree) // addCategorie sans « s » !
  	{
    	$this->tree[] = $tree;
  	}
	
   /**
    * Get tree
    *
    * @return Doctrine\Common\Collections\Collection
    */
	public function getTree() 
  	{
    	return $this->tree;
  	}
  	
  	
	/**
	* Add content
	*
	* @param Aio\ContentBundle\Entity\content $content
	*/
	public function addContent(\Aio\Bundle\ContentBundle\Entity\content $content) // addCategorie sans « s » !
  	{
    	$this->content[] = $content;
  	}
	
	 /**
    * Get content
    *
    * @return Doctrine\Common\Collections\Collection
    */
	public function getContent() 
  	{
    	return $this->content;
  	}
	
	public function removeContent(\Aio\Bundle\ContentBundle\Entity\content $content)
	{
		$this->content->removeElement($content);
	}

    /**
     * Set name
     *
     * @param string $name
     * @return Area
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Area
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
