<?php

namespace Aio\Bundle\PagesTeasersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AreaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', 'choice', array(
            	'choices' => array('structure' => 'Structure', 'content' => 'Content')
			))
 			->add('tree')
   	    ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aio\Bundle\PagesTeasersBundle\Entity\Area'
        ));
    }

    public function getName()
    {
        return 'aio_bundle_pagesteasersbundle_areatype';
    }
}
